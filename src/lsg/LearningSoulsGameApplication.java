package lsg;

import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import lsg.characters.Character;
import lsg.characters.Hero;
import lsg.characters.Zombie;
import lsg.consumables.food.SuperBerry;
import lsg.exceptions.*;
import lsg.graphics.CSSFactory;
import lsg.graphics.ImageFactory;
import lsg.graphics.panes.AnimationPane;
import lsg.graphics.panes.CreationPane;
import lsg.graphics.panes.HUDPane;
import lsg.graphics.panes.TitlePane;
import lsg.graphics.widgets.characters.renderers.CharacterRenderer;
import lsg.graphics.widgets.characters.renderers.HeroRenderer;
import lsg.graphics.widgets.characters.renderers.ZombieRenderer;
import lsg.graphics.widgets.characters.statbars.StatBar;
import lsg.graphics.widgets.skills.SkillBar;
import lsg.weapons.Sword;

public class LearningSoulsGameApplication extends Application{

    public static final String TITLE = "Learning Souls Game" ;

    public static final double DEFAULT_SCENE_WIDTH = 1200 ;
    public static final double DEFAULT_SCENE_HEIGHT = 800 ;

    private Scene scene ;
    private AnchorPane root;

    private TitlePane gameTitle ;
    private CreationPane creationPane ;
    private AnimationPane animationPane ;
    private HUDPane hudPane ;

    private String heroName ;

    private Hero hero ;
    private HeroRenderer heroRenderer ;

    private Zombie zombie ;
    private ZombieRenderer zombieRenderer ;
    private SkillBar skillBar;

    private final BooleanProperty heroCanPlay = new SimpleBooleanProperty(false);
    private IntegerProperty score = new SimpleIntegerProperty();

    @Override
    public void start(Stage stage) {
        stage.setTitle(TITLE);
        root = new AnchorPane() ;
        scene = new Scene(root, DEFAULT_SCENE_WIDTH, DEFAULT_SCENE_HEIGHT);

        stage.setResizable(false);
        stage.setScene(scene);
        buildUI() ;
        addListeners() ;

        stage.show();

        startGame() ;
    }

    private void addListeners(){
        creationPane.getNameField().setOnAction((event -> {
            heroName = creationPane.getNameField().getText() ;
            System.out.println("Nom du héro : " + heroName);
            if(!heroName.isEmpty()){
                root.getChildren().remove(creationPane);
                gameTitle.zoomOut((event1 -> play()));
            }
        }));
    }

    private void startGame(){
        gameTitle.zoomIn((event -> creationPane.fadeIn((event1 -> ImageFactory.preloadAll((() -> System.out.println("Pré-chargement des images terminé")))))));
    }

    private void play(){
        root.getChildren().add(animationPane) ;
        root.getChildren().add(hudPane) ;
        createHero();
        createSkills();
        createMonster((event -> {
            System.out.println("FIGHT !");
            hudPane.getMessagePane().showMessage("FIGHT !");
            heroCanPlay.setValue(true);
            hudPane.scoreProperty().bind(score);

        }));
    }

    private void createHero(){
        hero = new Hero(heroName) ;
        hero.setWeapon(new Sword());

        hero.setConsumable(new SuperBerry());

        heroRenderer = animationPane.createHeroRenderer() ;
        heroRenderer.goTo(root.getWidth()*0.5 - heroRenderer.getFitWidth()*0.65, null);

        StatBar bar = hudPane.getHeroStatBar() ;
        bar.getAvatar().setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.HERO_HEAD)[0]);
        bar.getName().setText(hero.getName());

        hudPane.getHeroStatBar().getLifeBar().progressProperty().bind(hero.lifeRateProperty());

        bar.getLifeBar().progressProperty().bind(hero.lifeRateProperty());
        bar.getStamBar().progressProperty().bind(hero.staminaRateProperty());
    }

    private void createMonster(EventHandler<ActionEvent> finishedHandler){
        zombie = new Zombie() ;
        zombieRenderer = animationPane.createZombieRenderer() ;
        zombieRenderer.goTo(root.getWidth()*0.5 - zombieRenderer.getBoundsInLocal().getWidth() * 0.15, finishedHandler);

        StatBar bar = hudPane.getMonserStatBar();
        bar.getAvatar().setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.ZOMBIE_HEAD)[0]);
        bar.getAvatar().setRotate(30);
        bar.getName().setText(zombie.getName());
        bar.getLifeBar().progressProperty().bind(zombie.lifeRateProperty());
        bar.getStamBar().progressProperty().bind(zombie.staminaRateProperty());
    }

    private void buildUI(){
        scene.getStylesheets().add(CSSFactory.getStyleSheet("LSG.css")) ;

        gameTitle = new TitlePane(scene, LearningSoulsGameApplication.TITLE) ;
        AnchorPane.setTopAnchor(gameTitle, 0.0);
        AnchorPane.setLeftAnchor(gameTitle, 0.0);
        AnchorPane.setRightAnchor(gameTitle, 0.0);
        root.getChildren().addAll(gameTitle) ;

        creationPane = new CreationPane() ;
        creationPane.setOpacity(0);
        AnchorPane.setTopAnchor(creationPane, 0.0);
        AnchorPane.setLeftAnchor(creationPane, 0.0);
        AnchorPane.setRightAnchor(creationPane, 0.0);
        AnchorPane.setBottomAnchor(creationPane, 0.0);
        root.getChildren().addAll(creationPane) ;

        animationPane = new AnimationPane(root) ;

        hudPane = new HUDPane() ;
        AnchorPane.setTopAnchor(hudPane, 0.0);
        AnchorPane.setLeftAnchor(hudPane, 0.0);
        AnchorPane.setRightAnchor(hudPane, 0.0);
        AnchorPane.setBottomAnchor(hudPane, 0.0);

    }

    private	void createSkills(){

        skillBar = hudPane.getSkillBar();
        skillBar.setDisable(!heroCanPlay.getValue());
        skillBar.getTrigger(0).setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.ATTACK_SKILL)[0]);
        skillBar.getTrigger(1).setImage(ImageFactory.getSprites(ImageFactory.SPRITES_ID.RECUPERATE_SKILL)[0]);

        skillBar.getTrigger(0).addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            skillBar.process(skillBar.getTrigger(0).getKeyCode());
            heroAttack();
        });

        skillBar.getTrigger(1).addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            skillBar.process(skillBar.getTrigger(1).getKeyCode());
            heroRecuperate();
        });

        skillBar.getConsumableTrigger().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            skillBar.process(skillBar.getConsumableTrigger().getKeyCode());
            heroConsume();
        });


        this.scene.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if(event.getCode() == skillBar.getTrigger(0).getKeyCode() && !skillBar.isDisabled()){
                skillBar.process(skillBar.getTrigger(0).getKeyCode());
                heroAttack();
            }
        });

        this.scene.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if(event.getCode() == skillBar.getTrigger(1).getKeyCode() && !skillBar.isDisabled()){
                skillBar.process(skillBar.getTrigger(1).getKeyCode());
                heroRecuperate();
            }
        });

        this.scene.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if(event.getCode() == skillBar.getConsumableTrigger().getKeyCode() && !skillBar.isDisabled() && !hero.getConsumable().isEmpty()){
                skillBar.process(skillBar.getConsumableTrigger().getKeyCode());
                heroConsume();
            }
        });


        heroCanPlay.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                skillBar.setDisable(!newValue);
            }
        });

        skillBar.getConsumableTrigger().setConsumable(hero.getConsumable());

    }

    private void characterAttack(Character agressor, CharacterRenderer agressorR, Character target, CharacterRenderer targetR, EventHandler<ActionEvent> finishedHandler) {

        try {
            int damage = agressor.attack();

            agressorR.attack(event -> {
                target.getHitWith(damage);

                if (target.isAlive()) targetR.hurt(eventA -> {
                    finishedHandler.handle(eventA);
                });
                else targetR.die(eventD -> {
                    finishedHandler.handle(eventD);
                });
            });

        }catch(StaminaEmptyException e){
            hudPane.getMessagePane().showMessage("ACTION HAS NO EFFECT: no more stamina !!!");
            agressorR.attack(event -> finishedHandler.handle(event));
        }catch(WeaponNullException e){
            hudPane.getMessagePane().showMessage("WARNING : no weapon has been equiped !!!");
            agressorR.attack(event -> finishedHandler.handle(event));
        }catch (WeaponBrokenException e){
            hudPane.getMessagePane().showMessage("WARNING : " + e.getMessage());
            agressorR.attack(event -> finishedHandler.handle(event));
        }
    }

    private void heroAttack() {

        heroCanPlay.setValue(false);
        characterAttack(hero, heroRenderer, zombie, zombieRenderer, event -> {
            System.out.println("HERO ATTACK");
            finishTurn();
        });
    }

    private void monsterAttack(){
        characterAttack(zombie, zombieRenderer, hero, heroRenderer, event -> {
            if (hero.isAlive()) heroCanPlay.setValue(true);
            else gameOver();
        });
    }

    private void gameOver(){
        hudPane.getMessagePane().showMessage("GAME OVER");
    }

    private void finishTurn(){

        if (zombie.isAlive()) monsterAttack();
        else {
            score.setValue(score.getValue()+1);
            animationPane.getChildren().remove(zombieRenderer);
            createMonster(event -> monsterAttack());
        }

    }

    private void heroRecuperate(){

        heroCanPlay.setValue(false);
        hero.recuperate();
        finishTurn();
    }

    private  void heroConsume(){

        heroCanPlay.setValue(false);

        try{
            hero.consume();
        }catch (ConsumeNullException e){
        hudPane.getMessagePane().showMessage("IMPOSSIBLE ACTION : no consumable has been equiped !");
    }catch (ConsumeEmptyException e){
        hudPane.getMessagePane().showMessage("ACTION HAS NO EFFECT: " + e.getConsumable().getName() + " is empty !");
    }catch (ConsumeRepairNullWeaponException e){
        hudPane.getMessagePane().showMessage("IMPOSSIBLE ACTION : no weapon has been equiped !");
    }

        finishTurn();
    }

}
