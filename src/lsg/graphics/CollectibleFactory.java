package lsg.graphics;

import javafx.scene.image.Image;
import lsg.bags.Collectible;
import lsg.consumables.drinks.SmallStamPotion;
import lsg.consumables.food.SuperBerry;

public class CollectibleFactory {

    public static Image getImageFor(Collectible collectible){
        if (collectible instanceof SmallStamPotion){
            return ImageFactory.getSprites(ImageFactory.SPRITES_ID.SMALL_STAM_POTION)[0];
        }else if (collectible instanceof SuperBerry){
            return ImageFactory.getSprites(ImageFactory.SPRITES_ID.SUPER_BERRY)[0];
        }else return null;
    }
}
