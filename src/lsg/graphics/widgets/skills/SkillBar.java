package lsg.graphics.widgets.skills;

import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Rectangle;

import java.util.LinkedHashMap;
import java.util.Map;

public class SkillBar extends HBox {

    private	SkillTrigger triggers[];
    private ConsumableTrigger consumableTrigger = new ConsumableTrigger(KeyCode.C, "c", null, null);

    public SkillBar(){

        this.setSpacing(10);
        this.prefWidth(110);
        this.prefHeight(110);
        this.setAlignment(Pos.CENTER);

        init();
    }

    private static LinkedHashMap<KeyCode, String> DEFAULT_BINDING = new LinkedHashMap<>();
    static {
        DEFAULT_BINDING.put(KeyCode.DIGIT1, "&");
        DEFAULT_BINDING.put(KeyCode.DIGIT2, "é");
        DEFAULT_BINDING.put(KeyCode.DIGIT3, "\"");
        DEFAULT_BINDING.put(KeyCode.DIGIT4, "'");
        DEFAULT_BINDING.put(KeyCode.DIGIT5, "(");
    }

    private void init(){

        triggers = new SkillTrigger[DEFAULT_BINDING.size()];

        int cpt = 0;
        for (Map.Entry<KeyCode, String > binding : DEFAULT_BINDING.entrySet()){
            triggers[cpt] = new SkillTrigger(binding.getKey(), binding.getValue(), null, null);
            this.getChildren().add(triggers[cpt]);
            cpt ++;
        }

        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(30);
        this.getChildren().add(rectangle);
        this.getChildren().add(consumableTrigger);
    }


    public SkillTrigger getTrigger(int slot){ return triggers[slot]; }

    public ConsumableTrigger getConsumableTrigger() { return consumableTrigger; }

    public void	process(KeyCode	code){

        if (!this.isDisabled()){

            if (consumableTrigger.getKeyCode().equals(code)) consumableTrigger.trigger();

            for (SkillTrigger trigger : triggers){
                if (trigger.getKeyCode().equals(code)){
                    trigger.trigger();
                    break;
                }
            }
        }
    }
}


