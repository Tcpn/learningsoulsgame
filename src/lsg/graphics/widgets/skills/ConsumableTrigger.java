package lsg.graphics.widgets.skills;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.input.KeyCode;
import lsg.consumables.Consumable;
import lsg.graphics.CSSFactory;
import lsg.graphics.CollectibleFactory;

public class ConsumableTrigger extends SkillTrigger {

    private Consumable consumable;

    public ConsumableTrigger(KeyCode keyCode, String text, SkillAction action, Consumable consumable) {
        super(keyCode, text, null, action);
        this.getStylesheets().add(CSSFactory.getStyleSheet("ConsumableTrigger.css"));
        this.getStyleClass().addAll("consumable");
        setConsumable(consumable);
    }

    public void setConsumable(Consumable consumable) {
        if (consumable == null ) return;
        this.consumable = consumable;
        this.consumable.isEmptyProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                setDisable(newValue);
            }
        });
        setImage(CollectibleFactory.getImageFor(consumable));
    }
}
