package lsg.graphics.widgets.skills;

import javafx.animation.ScaleTransition;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import lsg.graphics.CSSFactory;

public class SkillTrigger extends AnchorPane {

    private ImageView view;
    private Label text;
    private KeyCode keyCode;
    private SkillAction action;
    private ColorAdjust desaturate;

    SkillTrigger(KeyCode keyCode, String text, Image image, SkillAction	action){

        view = new ImageView();
        setKeyCode(keyCode);
        setText(new Label(text));
        view.setImage(image);
        setAction(action);
        desaturate = new ColorAdjust();
        desaturate.setSaturation(-1);
        desaturate.setBrightness(0.6);

        buildUI();
        addListeners();
    }

    private void buildUI(){

        this.getStylesheets().add(CSSFactory.getStyleSheet("SkillTrigger.css"));
        this.getStyleClass().addAll("skill", "skill-fx");


        view.setFitHeight(50);
        view.setFitWidth(50);

        text.getStyleClass().add("skill-text");
        text.setAlignment(Pos.CENTER);

        this.getChildren().add(view);

        AnchorPane.setTopAnchor(view, 0.0);
        AnchorPane.setLeftAnchor(view, 0.0);
        AnchorPane.setRightAnchor(view, 0.0);
        AnchorPane.setBottomAnchor(view, 0.0);

        this.getChildren().add(text);

        AnchorPane.setTopAnchor(text, 0.0);
        AnchorPane.setLeftAnchor(text, 0.0);
        AnchorPane.setRightAnchor(text, 0.0);
        AnchorPane.setBottomAnchor(text, 0.0);
    }

    public void trigger(){
        if (!this.isDisabled()){
            this.animate();
            if(action != null ) action.execute();
        }
    }

    public void addListeners(){
        this.onMouseClickedProperty().addListener(event ->{
            trigger();
        });

        this.disabledProperty().addListener(event1 ->{
            if (disabledProperty().getValue()) setEffect(desaturate);
            else setEffect(null);

        });
    }

    private void animate(){
        ScaleTransition st = new ScaleTransition(Duration.millis(100), this);
        st.setToX(1.3);
        st.setToY(1.3);
        st.setAutoReverse(true);
        st.setCycleCount(2);
        st.play();
    }

    public Label getText() { return text; }

    public void setText(Label text) { this.text = text; }

    public KeyCode getKeyCode() { return keyCode; }

    public void setKeyCode(KeyCode keyCode) { this.keyCode = keyCode; }

    public SkillAction getAction() { return action; }

    public void setAction(SkillAction action) { this.action = action; }

    public Image getImage(){ return view.getImage(); }

    public void setImage(Image image){ view.setImage(image); }

}
