package lsg.graphics.panes;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import lsg.graphics.widgets.characters.statbars.StatBar;
import lsg.graphics.widgets.skills.SkillBar;
import lsg.graphics.widgets.texts.GameLabel;

public class HUDPane extends BorderPane {

    private MessagePane messagePane ;
    private StatBar heroStatBar, monserStatBar ;
    private SkillBar skillBar;
    private IntegerProperty score = new SimpleIntegerProperty();
    private GameLabel scoreLabel;

    public HUDPane() {
        this.setPadding(new Insets(80, 10, 10, 10));
        buildCenter() ;
        buildTop() ;
        buildBottom();
        score.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                scoreLabel.setText(newValue.toString());
            }
        });
    }

    public MessagePane getMessagePane() {
        return messagePane;
    }

    public StatBar getHeroStatBar() {
        return heroStatBar;
    }

    public StatBar getMonserStatBar() {
        return monserStatBar;
    }

    public SkillBar getSkillBar() { return skillBar; }

    public int getScore() { return score.get(); }

    public IntegerProperty scoreProperty() {
        return score;
    }

    private void buildTop(){
        BorderPane borderPane = new BorderPane() ;
        this.setTop(borderPane);

        heroStatBar = new StatBar() ;
        borderPane.setLeft(heroStatBar);

        monserStatBar = new StatBar() ;
        borderPane.setRight(monserStatBar);
        monserStatBar.flip();

        scoreLabel = new GameLabel();
        borderPane.setCenter(scoreLabel);
        scoreLabel.setText("0");
        scoreLabel.setScaleX(1.3);
        scoreLabel.setScaleY(1.3);
        scoreLabel.setTranslateY(40);
    }

    private void buildCenter(){
        messagePane = new MessagePane() ;
        this.setCenter(messagePane);
    }

    private	void buildBottom(){
        skillBar = new SkillBar();
        this.setBottom(skillBar);
    }
}
